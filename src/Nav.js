import React from 'react';
import { Link } from "react-router-dom";

export default function Nav() {
	return (
		<nav className="navbar navbar-dark bg-dark">
			<button className="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
				<span className="navbar-toggler-icon"></span>
			</button>
			<a className="navbar-brand" href="#!">Navbar</a>

			<div className="navbar-collapse collapse" id="navbarColor01">
				<ul className="navbar-nav mr-auto">
					<li className="nav-item active">
						<Link to="/">Home </Link>
					</li>	
					<li className="nav-item dropdown">
						<a className="nav-link dropdown-toggle" data-toggle="dropdown"
							href="#" role="button" aria-haspopup="true" aria-expanded="false">
							Catalog</a>
						<div className="dropdown-menu">
							<Link className="dropdown-item" to="/ford">Ford</Link>
							<Link className="dropdown-item" to="/chevrolet">Chevrolet</Link>
							<Link className="dropdown-item" to="/toyota">Toyota</Link>
						</div>
					</li>
					<li className="nav-item">
						<Link to="/narnia">News</Link>
					</li>
					<li className="nav-item">
						<Link to="/about">About</Link>
					</li>
				</ul>
			</div>
		</nav>
	);
}