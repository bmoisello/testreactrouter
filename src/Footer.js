import React from 'react';

export default function Footer() {
	return (
		<footer className="page-footer font-small bg-light">
			<div className="container">
				<div className="row">
					<div className="col-md-12 py-5 d-flex justify-content-center">
						<div className="mb-5">
								<i className="fab fa-facebook-f fa-lg white-text fa-2x"> </i>
								<i className="fab fa-twitter fa-lg white-text fa-2x"> </i>
								<i className="fab fa-google-plus-g fa-lg white-text fa-2x"> </i>
								<i className="fab fa-linkedin-in fa-lg white-text fa-2x"> </i>
								<i className="fab fa-instagram fa-lg white-text fa-2x"> </i>
								<i className="fab fa-pinterest fa-lg white-text fa-2x"> </i>
						</div>
					</div>
				</div>
			</div>
			<div className="footer-copyright text-center py-3">© 2018 Copyright:
    		<a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
			</div>
		</footer>
	);
}