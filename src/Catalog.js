import React from 'react';

export default class Catalog extends React.Component {
	constructor(props) {
		super(props);
		this.catalogChevrolet = [
			{
				image: "https://www.agenciasdemedios.com.ar/wp-content/uploads/2017/09/Instagram-links-ads-300x300.jpg",
				description: "Onix LTZ",
			},
			{
				image: "https://www.actualidadmotor.com/wp-content/uploads/2015/07/chevrolet-colorado-300x300.jpg",
				description: "S10 - Duramax",
			},
			{
				image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSV4pPnWIJXHrYDTcpje2vFXJivtM_HmFpsFhUAQZScMJfKuYAW",
				description: "Tracker LTZ AT",
			}
		];
		this.catalogFord = [
			{
				image: "https://www.murcia4rentacar.com/wp-content/uploads/2018/09/ford_fiesta_azul-300x300.jpg",
				description: "Fiesta SE",
			},
			{
				image: "https://sabritasventas.com/wp-content/uploads/2019/09/f03-300x300.png",
				description: "EcoSport SE",
			},
			{
				image: "https://www.murcia4rentacar.com/wp-content/uploads/2018/09/ford_focus_amarillo-300x300.jpg",
				description: "Focus SE",
			}
		];
		this.catalogToyota = [
			{
				image: "https://sabritasventas.com/wp-content/uploads/2019/02/hilux-4-300x300.jpg",
				description: "Hilux SEG",
			},
			{
				image: "https://sotaroautosyplanes.com.ar/wp-content/uploads/2019/02/Toyota-corolla-300x300.jpg",
				description: "Corolla SEG",
			},
			{
				image: "https://sotaroautosyplanes.com.ar/wp-content/uploads/2019/02/Toyota-etyos-300x300.jpg",
				description: "Etios XEI",
			}
		];
	}

	renderCatalog(element) {
		return (
				<div className="card-body col-12 col-lg-4">
					<img class="card-img-top" src={element.image} alt="Card cap" />
					<div class="card-body text-center"> {element.description} </div>
				</div>
		);
	}

	renderSwitch() {
		const catalog = this.props.location.pathname;
		switch (catalog) {
			case "/ford":
				return this.catalogFord.map((element) => this.renderCatalog(element));
			case "/chevrolet":
					return this.catalogChevrolet.map((element) => this.renderCatalog(element));
			case "/toyota":
					return this.catalogToyota.map((element) => this.renderCatalog(element));
			default:
				break;
		}
	}

	render() {
		return (
			<div className="container-fluid">
				<div className="row">
					{this.renderSwitch()}
				</div>
			</div>
		);
	}
}