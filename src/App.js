import React from 'react';
import Nav from './Nav';
import Home from './Home';
import Catalog from './Catalog';
import Auto from './Components/Auto/Auto';
import Footer from './Footer';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="">
        <Nav />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/ford" component={Catalog} />
          <Route path="/chevrolet" component={Catalog} />
          <Route path="/toyota" component={Catalog} />
          <Route path="/:auto" component={Auto} />
        </Switch>
        <Footer />        
      </div>
    </Router>
  );
}

export default App;
