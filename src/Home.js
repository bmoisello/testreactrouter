import React from 'react';

export default function Home() {
	return (
		<div className="bd-example">
			<div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">
				<ol className="carousel-indicators">
					<li data-target="#carouselExampleCaptions" data-slide-to="0" className="active"></li>
					<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
				</ol>
				<div className="carousel-inner" role="listbox">
					<div className="carousel-item active">
						<img className="d-block w-100" src="https://loff.it/wp-content/uploads/2016/03/loffit-morgan-ev3-un-nuevo-clasico-electrico-01-900x300.jpg" />
					</div>
					<div className="carousel-item">
						<img className="d-block w-100" src="https://tincargarage.com/wp-content/uploads/2017/01/Classic-Car-Cadillac-900x300.jpg" />
					</div>
				</div>
				<a className="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
					<span className="carousel-control-prev-icon" aria-hidden="true"></span>
					<span className="sr-only">Previous</span>
				</a>
				<a className="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
					<span className="carousel-control-next-icon" aria-hidden="true"></span>
					<span className="sr-only">Next</span>
				</a>
			</div>
		</div>
	);
}